"use strict";
class Animal {
    eat() {
        console.log("Animal Eats");
    }
}
class Bird extends Animal {
    fly() {
        console.log("Birds can fly");
    }
}
const parrot = new Bird(); // Parrot also a bird.
parrot.eat();
parrot.fly();
const penguin = new Bird();
penguin.eat();
penguin.fly(); // But penguin can't fly. I wrote the correct one inside of cleanCode.ts
