"use strict";
class Animal {
    eat() {
        console.log("Animal Eats");
    }
}
class Bird extends Animal {
    fly() {
        console.log("Birds can fly");
    }
}
const parrot = new Bird(); // Parrot also a bird.
parrot.eat();
parrot.fly();
class Penguin extends Animal {
    walk() {
        console.log("Penguin walks");
    }
}
