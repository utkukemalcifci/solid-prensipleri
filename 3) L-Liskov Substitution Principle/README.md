### Liskov Substitution Principle

According to this principle a subclass should provide all features of the parent class. If the subclass cannot provide a certain feature, then a new class should be created.