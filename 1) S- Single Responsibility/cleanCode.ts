import logger from "./logger";

class CalorieTracker {
  private maxCalories: number;
  private currentCalories: number;
  constructor(maxCalories: number) {
    this.maxCalories = maxCalories;
    this.currentCalories = 0;
  }

  trackCalories(calorieCount: number) {
    this.currentCalories += calorieCount;
    if (this.currentCalories > this.maxCalories) {
      logger('You recieve max calories');
    }
  }

}

const calorieTracker = new CalorieTracker(2000);
calorieTracker.trackCalories(2100);
