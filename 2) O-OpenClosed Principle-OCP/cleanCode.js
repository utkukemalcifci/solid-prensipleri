class Question {
  constructor(description) {
    this.description = description;
  }
}

class BoolenTypeQuestions extends Question {
  print() {
    console.log(this.description);
    console.log("1. True");
    console.log("2. False");
  }
}

class MultiSelectQuestions extends Question {
  constructor(description, options) {
    super(description), (this.options = options);
  }
  print() {
    console.log(this.description);
    this.options.forEach((question, index) => {
      console.log(`${index + 1}. ${question}`);
    });
  }
}

class Text extends Question {
  print() {
    console.log(this.description);
    console.log("Cevap: ______________");
  }
}

class Range extends Question {
  print() {
    console.log(this.description);
    console.log("Minimum:_____________");
    console.log("Maximum:_____________");
  }
}

function writeQuestions(questions) {
  questions.forEach((question) => {
    question.print();
  });
}

const questions = [
  new BoolenTypeQuestions("Is this example helpful for you "),
  new Range(
    "Can you write the maximum and minimum temperature of your country for yesterday ?"
  ),
  new MultiSelectQuestions("Who is your favorite singer ?", [
    "Micheal Jackson",
    "Athena",
    "Queen",
    "Lapsekili Tayfur",
  ]),
  new Text("Please explain SOLID principles."),
];

writeQuestions(questions);

