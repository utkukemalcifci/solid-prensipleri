function writeQuestions(questions) {
  questions.forEach((question) => {
    console.log(question.description);
    switch (question.type) {
      case "boolean":
        console.log("1. True");
        console.log("2. False");
        break;
      case "multipleChoice":
        question.options.forEach((option, index) => {
          console.log(`${index + 1}. ${option}`);
        });
        break;
      case "text":
        console.log("Answer: ______________");
        break;
      case "range":
        console.log("Minimum:_____________");
        console.log("Maximum:_____________");
        break;
    }
  });
}

const questions = [
  {
    type: "boolean",
    description: "Is this example helpful for you ?",
  },
  {
    type: "multipleChoice",
    description: "Who is your favorite singer ?",
    options: ["Micheal Jackson", "Athena", "Queen", "Lapsekili Tayfur"],
  },
  {
    type: "text",
    description: "Please explain SOLID principles.",
  },
  {
    type: "range",
    description:
      "Can you write the maximum and minimum temperature of your country for yesterday ?",
  },
];

writeQuestions(questions);
