class UserService {
    private database: Database;

    constructor() {
        this.database = new Database();
    }

    getUserData(userId: string) {
        return this.database.getUserData(userId);
    }
}

class Database {
    getUserData(userId: string) {
        // Veritabanından kullanıcı verilerini çek ve döndür
    }
}

const userService = new UserService();
const userData = userService.getUserData("123");
