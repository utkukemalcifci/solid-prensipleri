interface DatabaseInterface {
    getUserData(userId: string): any;
}

class Database implements DatabaseInterface {
    getUserData(userId: string) {
        // Get users from db
    }
}

class UserService {
    private database: DatabaseInterface;

    constructor(database: DatabaseInterface) {
        this.database = database;
    }

    getUserData(userId: string) {
        return this.database.getUserData(userId);
    }
}

const database = new Database();
const userService = new UserService(database);
const userData = userService.getUserData("123");
