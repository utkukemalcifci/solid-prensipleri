"use strict";
class Human {
    work() {
        console.log("Working...");
    }
    eat() {
        console.log("Eating...");
    }
}
class Robot {
    work() {
        console.log("Working...");
    }
}
const robot = new Robot();
robot.work();
