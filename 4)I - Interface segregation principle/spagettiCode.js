"use strict";
class Human {
    work() {
        console.log("Working...");
    }
    eat() {
        console.log("Eating...");
    }
}
class Robot {
    work() {
        console.log("Working...");
    }
    eat() {
        // Robot can't eat. Doesnt matter this function.
    }
}
