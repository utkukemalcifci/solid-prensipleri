interface Worker {
  work(): void;
  eat(): void;
}

class Human implements Worker {
  work() {
      console.log("Working...");
  }
  
  eat() {
      console.log("Eating...");
  }
}

class Robot implements Worker {
  work() {
      console.log("Working...");
  }
  
  eat() {
      // Robot can't eat. Doesnt matter this function.
  }
}
